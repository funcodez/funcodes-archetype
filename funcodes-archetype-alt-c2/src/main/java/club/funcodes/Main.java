// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is written and provided by Siegfried Steiner, Munich, Germany.
// Feel free to use it as skeleton for your own applications. Make sure you have
// considered the license conditions of the included artifacts (pom.xml).
// -----------------------------------------------------------------------------
// The REFCODES.ORG artifacts used by this template are copyright (c) by
// Siegfried Steiner, Munich, Germany and licensed under the following
// (see "http://en.wikipedia.org/wiki/Multi-licensing") licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes;

import static org.refcodes.cli.CliSugar.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import org.refcodes.archetype.C2Helper;
import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.ConfigOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.Literal;
import org.refcodes.data.Scheme;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.properties.Properties;
import org.refcodes.properties.PropertiesBuilderImpl;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.rest.HttpRestClient;
import org.refcodes.rest.HttpRestServer;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.web.HttpResponse;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.MediaType;
import org.refcodes.web.RequestHeaderFields;

/**
 * A minimum REFCODES.ORG enabled Command and Control (C2) daemon and client
 * application. Get inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	// See "http://www.refcodes.org/blog/logging_like_the_nerds_log" |-->
	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();
	// <--| See "http://www.refcodes.org/blog/logging_like_the_nerds_log"

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "funcodes-archetype-alt-c2";
	private static final String TITLE = ( NAME.lastIndexOf( '-' ) != -1 ? "<" + NAME.substring( NAME.lastIndexOf( '-' ) + 1 ) + ">" : NAME ).toUpperCase();
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String DESCRIPTION = "A minimum REFCODES.ORG enabled Command and Control (C2) daemon and client application. Get inspired by [https://bitbucket.org/funcodez].";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES (see [https://www.funcodes.club])";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String MESSAGE_PROPERTY = "message";
	private static final String LISTEN_PROPERTY = "listen";
	private static final String SEND_PROPERTY = "send";
	private static final String C2_PORT_PROPERTY = "port";
	private static final int MAX_OPEN_SEND_DAEMON_RETRY_COUNT = 5;
	private static final String SEND_ENDPOINT = "/send";
	private static final String SEND_PROMPT = "$> ";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static C2Helper _c2Helper = null;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		// See "http://www.refcodes.org/refcodes/refcodes-cli" |-->

		final Flag theSendFlag = flag( 'S', "send", SEND_PROPERTY, "Send data." );
		final Flag theListenFlag = flag( 'L', "listen", LISTEN_PROPERTY, "Listen for data." );
		final StringOption theMessageArg = stringOption( 'm', "message", MESSAGE_PROPERTY, "The message to be sent." );
		final Flag theSysInfoFlag = sysInfoFlag();
		final Flag theHelpFlag = helpFlag();
		final Flag theQuietFlag = quietFlag();
		final Flag theDebugFlag = debugFlag();
		final Flag theInitFlag = initFlag();
		final Flag theCleanFlag = cleanFlag();
		final ConfigOption theConfigArg = configOption();

		// @formatter:off
		final Term theArgsSyntax = cases(
			and( xor( theListenFlag, and( theSendFlag, theMessageArg ) ), any ( theConfigArg, theQuietFlag, theDebugFlag ) ),
			and( theConfigArg, xor( theListenFlag, and( theSendFlag, theMessageArg ) ), any ( theQuietFlag, theDebugFlag ) ),
			and( theInitFlag, optional( theConfigArg, theQuietFlag ) ),
			and( theCleanFlag, any( theQuietFlag, theDebugFlag ) ),
			xor( theHelpFlag, and( theSysInfoFlag, any( theQuietFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "Listen for data", theListenFlag),
			example( "Send data", theSendFlag, theMessageArg),
			example( "Listen for data using a given config", theListenFlag, theConfigArg),
			example( "Send data using a given config", theSendFlag, theMessageArg, theConfigArg),
			example( "Initialize default config file", theInitFlag ),
			example( "Initialize specific config file", theInitFlag, theConfigArg),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// <--| See "http://www.refcodes.org/refcodes/refcodes-cli"

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isVerbose = theCliHelper.isVerbose();
		final boolean isDebug = theArgsProperties.getBoolean( theDebugFlag );

		if ( isVerbose ) {
			LOGGER.info( "Starting application <" + NAME + "> ..." );
		}

		if ( isDebug ) {
			LOGGER.info( "Additional debug output enabled ..." );
		}

		try {
			_c2Helper = C2Helper.builder().withInstanceAlias( "c2" ).withResourceClass( Main.class ).withLogger( LOGGER ).withVerbose( isVerbose ).build();
			if ( theCleanFlag.isEnabled() ) {
				_c2Helper.deleteLockFile( isVerbose );
			}
			else if ( theListenFlag.isEnabled() ) {
				listen( isVerbose, isDebug );
			}
			else if ( theSendFlag.isEnabled() ) {
				String theMessage = theMessageArg.getValue();
				sendDispatch( theMessage, isVerbose, isDebug );
			}
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}

	/**
	 * Determines whether to send directly (locally) or use a listener daemon
	 * (in case no listener is up and running) by testing a listener's lockfile
	 * {@link C2Helper#getLockFileName()} and on its existence trying to use the
	 * listener's daemon instead.
	 * 
	 * @param aMessage The message to be sent.
	 * @param isVerbose Be more verbose.
	 * @param isDebug Show stack traces in case of errors.
	 * 
	 * @throws IOException thrown in case there were I/O related problems (lock
	 *         file or HTTP).
	 * @throws HttpResponseException thrown in case of {@link HttpResponse}
	 *         related issues.
	 * @throws ParseException thrown in case parsing the lockfile's properties
	 *         failed.
	 */
	private static void sendDispatch( String aMessage, boolean isVerbose, boolean isDebug ) throws IOException, HttpResponseException, ParseException {
		final Properties theLockProperties = _c2Helper.readLockFile( isVerbose );
		if ( theLockProperties != null ) {
			final int thePort = theLockProperties.getInt( C2_PORT_PROPERTY );
			if ( PortManagerSingleton.getInstance().isPortBound( thePort ) ) {
				final HttpRestClient theClient = new HttpRestClient().withBaseUrl( Scheme.HTTP, Literal.LOCALHOST.getValue(), thePort );
				final RequestHeaderFields theHeadeFields = new RequestHeaderFields();
				theHeadeFields.putContentType( MediaType.TEXT_PLAIN );
				theClient.doPost( toSendEndpoint(), theHeadeFields, aMessage );
				if ( isVerbose ) {
					LOGGER.printSeparator();
					LOGGER.info( "Sending \"" + aMessage + "\" ..." );
					LOGGER.printTail();
				}
				return;
			}
		}
		if ( isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "No open C2-port detected, doing loopback by sending and receiving internally!" );
			LOGGER.printTail();
		}
		System.out.print( SEND_PROMPT );
		sendMessage( aMessage );
	}

	/**
	 * Starts the send daemon which creates a a lockfile containing the port
	 * being bound by the daemon.
	 * 
	 * @param isVerbose Be more verbose.
	 * @param isDebug Show stack traces in case of errors.
	 * 
	 * @throws IOException in case there were I/O problems reading the lockfile
	 *         or using a HTTP connection.
	 * @throws FileNotFoundException
	 * @throws ParseException thrown in case parsing the lockfile's properties
	 *         failed.
	 */
	private static void sendDaemon( boolean isVerbose, boolean isDebug ) throws IOException, ParseException {
		final Properties theProperties = _c2Helper.readLockFile( isVerbose );
		if ( theProperties != null ) {
			Integer thePort = theProperties.getInt( C2_PORT_PROPERTY );
			if ( thePort != null && !PortManagerSingleton.getInstance().isPortAvaialble( thePort ) ) {
				throw new IllegalStateException( "Cannot start daemon as the port <" + thePort + "> reserved by the lockfile is already in use!" );
			}
		}
		HttpRestServer theRestServer = null;
		int count = 1;
		int thePort;
		do {
			thePort = PortManagerSingleton.getInstance().bindAnyPort();
			try {
				theRestServer = new HttpRestServer().withOpen( thePort );
			}
			catch ( IOException e ) {
				if ( count > MAX_OPEN_SEND_DAEMON_RETRY_COUNT ) {
					throw e;
				}
			}
			count++;
		} while ( theRestServer == null );

		_c2Helper.writeLockFile( new PropertiesBuilderImpl().withPutInt( C2_PORT_PROPERTY, thePort ), isVerbose );
		theRestServer.onPost( toSendEndpoint(), ( aRequest, aResponse ) -> { sendMessage( aRequest.getHttpBody() ); System.out.print( SEND_PROMPT ); } ).open();
	}

	/**
	 * 
	 * @param aMessage
	 */
	private static void sendMessage( String aMessage ) {
		System.out.println( aMessage );
	}

	/**
	 * 
	 * @param isVerbose Be more verbose.
	 * @param isDebug Show stack traces in case of errors.
	 * 
	 * @throws IOException in case there were I/O problems reading the lockfile
	 *         or using a HTTP connection.
	 * @throws ParseException thrown in case parsing the lockfile's properties
	 *         failed.
	 */
	private static void listen( boolean isVerbose, boolean isDebug ) throws IOException, ParseException {
		sendDaemon( isVerbose, isDebug );
		if ( isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "Listening ..." );
			LOGGER.printTail();
		}
		System.out.print( SEND_PROMPT );
	}

	/**
	 * Constructs the endpoint taking the instance alias into account.
	 * 
	 * @return The according endpoint.
	 */
	private static String toSendEndpoint() {
		return SEND_ENDPOINT + _c2Helper.getInstanceAlias() != null ? ( "/" + _c2Helper.getInstanceAlias().toLowerCase() ) : "";
	}
}
