// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is written and provided by Siegfried Steiner, Munich, Germany.
// Feel free to use it as skeleton for your own applications. Make sure you have
// considered the license conditions of the included artifacts (pom.xml).
// -----------------------------------------------------------------------------
// The REFCODES.ORG artifacts used by this template are copyright (c) by
// Siegfried Steiner, Munich, Germany and licensed under the following
// (see "http://en.wikipedia.org/wiki/Multi-licensing") licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes;

import static org.refcodes.cli.CliSugar.*;
import static org.refcodes.tabular.TabularSugar.*;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.util.Date;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.ArrayOption;
import org.refcodes.cli.ConfigOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.Option;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.Delimiter;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.runtime.Execution;
import org.refcodes.tabular.CsvRecordReader;
import org.refcodes.tabular.CsvRecordWriter;
import org.refcodes.tabular.Header;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.time.DateFormat;

/**
 * A minimum REFCODES.ORG enabled command application processing CSV files. Get
 * inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	// See "http://www.refcodes.org/blog/logging_like_the_nerds_log" |-->
	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();
	// <--| See "http://www.refcodes.org/blog/logging_like_the_nerds_log"

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "funcodes-archetype-alt-csv";
	private static final String TITLE = ( NAME.lastIndexOf( '-' ) != -1 ? "<" + NAME.substring( NAME.lastIndexOf( '-' ) + 1 ) + ">" : NAME ).toUpperCase();
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String DESCRIPTION = "A minimum REFCODES.ORG enabled command application processing CSV files. Get inspired by [https://bitbucket.org/funcodez].";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES (see [https://www.funcodes.club])";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final char DEFAULT_DELIMITER_PROPERTY = Delimiter.CSV.getChar();
	private static final String DEST_DELIMITER_PROPERTY = "destinationDelimiter";
	private static final String DEST_FILE_PROPERTY = "destinationFile";
	private static final String HEADER_PROPERTY = "header";
	private static final String SOURCE_DELIMITER_PROPERTY = "sourceDelimiter";
	private static final String SOURCE_FILE_PROPERTY = "sourceFile";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		// See "http://www.refcodes.org/refcodes/refcodes-cli" |-->

		final Option<String> theSourcePathArg = stringOption( 's', "source-file", SOURCE_FILE_PROPERTY, "The source CSV file from which to read the values. If omitted, then <STDIN> is used." );
		final Option<Character> theSourceDelimiterArg = charOption( "source-delimiter", SOURCE_DELIMITER_PROPERTY, "The delimiter to use for parsing the source CSV file." );
		final Option<String> theDestPathArg = stringOption( 'd', "destination-file", DEST_FILE_PROPERTY, "The destination CSV file to which to write the values, if omitted, then <STDOUT> is used." );
		final Option<Character> theDestDelimiterArg = charOption( "destination-delimiter", DEST_DELIMITER_PROPERTY, "The delimiter to use when generating the destination CSV file." );
		final ArrayOption<String> theHeaderArg = asArray( stringOption( 'h', "header", HEADER_PROPERTY, "The header columns to use for the destination CSV file." ) );
		final ConfigOption theConfigOption = configOption();
		final Flag theInitFlag = initFlag();
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag();
		final Flag theHelpFlag = helpFlag();
		final Flag theDebugFlag = debugFlag();

		// @formatter:off
		final Term theArgsSyntax = cases (
			optional( theSourcePathArg, theSourceDelimiterArg, theDestPathArg, theDestDelimiterArg, theHeaderArg, theConfigOption, theVerboseFlag, theDebugFlag ),
			and( theInitFlag, optional( theConfigOption, theVerboseFlag, theDebugFlag) ),
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "Process source file to <STDOUT>", theSourcePathArg ),
			example( "Process source file's dedicated headers to <STDOUT>", theSourcePathArg, theHeaderArg ),
			example( "Specify source and destination delimiter", theSourceDelimiterArg, theDestDelimiterArg ),
			example( "Process source to destination file", theSourcePathArg, theDestPathArg ),
			example( "Initialize default config file", theInitFlag, theVerboseFlag),
			example( "Initialize specific config file", theConfigOption, theInitFlag, theVerboseFlag),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// <--| See "http://www.refcodes.org/refcodes/refcodes-cli"

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isVerbose = theCliHelper.isVerbose();
		final boolean isDebug = theArgsProperties.getBoolean( theDebugFlag );

		if ( isVerbose ) {
			LOGGER.info( "Starting application <" + NAME + "> ..." );
		}

		if ( isDebug ) {
			LOGGER.info( "Additional debug output enabled ..." );
		}

		if ( isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "Name: \"" + theArgsProperties.get( "application/name" ) + "\"" );
			LOGGER.info( "Company: \"" + theArgsProperties.get( "application/company" ) + "\"" );
			LOGGER.info( "Version: \"" + theArgsProperties.get( "application/version" ) + "\"" );
			LOGGER.printSeparator();
		}

		// ---------------------------------------------------------------------
		// CSV:
		// ---------------------------------------------------------------------

		try {
			final File theSourceFile = ( theSourcePathArg.hasValue() ) ? new File( theArgsProperties.get( SOURCE_FILE_PROPERTY ) ) : null;
			final File theDestFile = ( theDestPathArg.hasValue() ) ? new File( theArgsProperties.get( DEST_FILE_PROPERTY ) ) : null;
			final char theSourceDelimiter = theArgsProperties.getCharOr( theSourceDelimiterArg, DEFAULT_DELIMITER_PROPERTY );
			final char theDestDelimiter = theArgsProperties.getCharOr( theDestDelimiterArg, DEFAULT_DELIMITER_PROPERTY );
			if ( isVerbose ) {
				LOGGER.info( "Source file = <" + ( theSourceFile != null ? theSourceFile : "STDIN" ) + ">" );
				LOGGER.info( "Destination file = <" + ( theDestFile != null ? theDestFile : "STDOUT" ) + ">" );
				LOGGER.info( "Source delimiter = '" + theSourceDelimiter + "'" );
				LOGGER.info( "Destination delimiter = '" + theDestDelimiter + "'" );
			}
			final Header<?> theSourceHeader = headerOf( dateColumn( "date", DateFormat.MIN_DATE_FORMAT.getFormatter() ), intColumn( "n1" ), intColumn( "n2" ), intColumn( "n3" ), intColumn( "n4" ), intColumn( "n5" ), intColumn( "n6" ), floatColumn( "jackpot" ), stringColumn( "currency" ) );
			Header<?> theHeader = theSourceHeader;
			if ( theHeaderArg.hasValue() ) {
				theHeader = headerOf( theHeaderArg.getValue() );
			}
			if ( isVerbose ) {
				LOGGER.info( "Header = " + VerboseTextBuilder.asString( theHeader.toKeys() ) );
			}
			if ( theDestFile == null ) {
				LOGGER.printTail(); // STDOUT will follow
			}
			int count = 0;
			Tupel eTupel;
			try ( CsvRecordReader<?> theCsvReader = theSourceFile != null ? new CsvRecordReader<>( theSourceHeader, theSourceFile, theSourceDelimiter ) : new CsvRecordReader<>( theSourceHeader, new BufferedInputStream( Execution.toBootstrapStandardIn() ), theSourceDelimiter ) ) {
				try ( CsvRecordWriter<?> theCsvWriter = theDestFile != null ? new CsvRecordWriter<>( theHeader, theDestFile, theDestDelimiter ) : new CsvRecordWriter<>( theHeader, new BufferedOutputStream( Execution.toBootstrapStandardOut() ), theDestDelimiter ) ) {
					theCsvReader.readHeader();
					theCsvWriter.writeHeader();
					while ( theCsvReader.hasNext() ) {
						eTupel = theCsvReader.nextType( Tupel.class );
						theCsvWriter.writeNextType( eTupel );
						count++;
					}
				}
			}

			if ( isVerbose && theDestFile != null ) {
				LOGGER.printSeparator();
				LOGGER.info( "Processed <" + count + "> number of lines!" );
			}
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class Tupel {

		Date date;
		int n1;
		int n2;
		int n3;
		int n4;
		int n5;
		int n6;
		float jackpot;
		String currency;

		public Tupel() {}

		public Tupel( Date date, int n1, int n2, int n3, int n4, int n5, int n6, float jackpot, String currency ) {
			this.date = date;
			this.n1 = n1;
			this.n2 = n2;
			this.n3 = n3;
			this.n4 = n4;
			this.n5 = n5;
			this.n6 = n6;
			this.jackpot = jackpot;
			this.currency = currency;
		}

		public Date getDate() {
			return date;
		}

		public void setDate( Date date ) {
			this.date = date;
		}

		public int getN1() {
			return n1;
		}

		public void setN1( int n1 ) {
			this.n1 = n1;
		}

		public int getN2() {
			return n2;
		}

		public void setN2( int n2 ) {
			this.n2 = n2;
		}

		public int getN3() {
			return n3;
		}

		public void setN3( int n3 ) {
			this.n3 = n3;
		}

		public int getN4() {
			return n4;
		}

		public void setN4( int n4 ) {
			this.n4 = n4;
		}

		public int getN5() {
			return n5;
		}

		public void setN5( int n5 ) {
			this.n5 = n5;
		}

		public int getN6() {
			return n6;
		}

		public void setN6( int n6 ) {
			this.n6 = n6;
		}

		public float getJackpot() {
			return jackpot;
		}

		public void setJackpot( float jackpot ) {
			this.jackpot = jackpot;
		}

		public String getCurrency() {
			return currency;
		}

		public void setCurrency( String currency ) {
			this.currency = currency;
		}
	}
}
