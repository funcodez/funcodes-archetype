// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is written and provided by Siegfried Steiner, Munich, Germany.
// Feel free to use it as skeleton for your own applications. Make sure you have
// considered the license conditions of the included artifacts (pom.xml).
// -----------------------------------------------------------------------------
// The REFCODES.ORG artifacts used by this template are copyright (c) by
// Siegfried Steiner, Munich, Germany and licensed under the following
// (see "http://en.wikipedia.org/wiki/Multi-licensing") licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes;

import static org.refcodes.cli.CliSugar.*;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.ConfigOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.IntOption;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.Scheme;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.rest.HttpExceptionHandler;
import org.refcodes.rest.HttpRestClient;
import org.refcodes.rest.HttpRestServer;
import org.refcodes.rest.RestRequestEvent;
import org.refcodes.rest.RestResponse;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.web.HttpBodyMap;
import org.refcodes.web.HttpServerResponse;
import org.refcodes.web.HttpStatusCode;

/**
 * A minimum REFCODES.ORG enabled HTTP driven command line interface (CLI)
 * application. Get inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	// See "http://www.refcodes.org/blog/logging_like_the_nerds_log" |-->
	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();
	// <--| See "http://www.refcodes.org/blog/logging_like_the_nerds_log"

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "funcodes-archetype-alt-rest";
	private static final String TITLE = ( NAME.lastIndexOf( '-' ) != -1 ? "<" + NAME.substring( NAME.lastIndexOf( '-' ) + 1 ) + ">" : NAME ).toUpperCase();
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String DESCRIPTION = "A minimum REFCODES.ORG enabled HTTP driven command line interface (CLI) application. Get inspired by [https://bitbucket.org/funcodez].";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES (see [https://www.funcodes.club])";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final int DEFAULT_PORT = 8080;
	private static final String DEFAULT_HOST = "localhost";
	private static final String PING_PROPERTY = "ping";
	private static final String HOST_PROPERTY = "host";
	private static final String PORT_PROPERTY = "port";
	private static final String SERVER_PROPERTY = "server";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		// See "http://www.refcodes.org/refcodes/refcodes-cli" |-->

		final StringOption thePingOption = stringOption( "ping", PING_PROPERTY, "Sends an HTTP GET-Request (ping) to be echoed (pong) by the RESTful server." );
		final StringOption theHostOption = stringOption( "host", HOST_PROPERTY, "The host on which the RESTful server is running (defaults to \"localhost\")." );
		final IntOption thePortOption = intOption( 'p', "port", PORT_PROPERTY, "The TCP port to be used for the HTTP echo." );
		final Flag theServerFlag = flag( "server", SERVER_PROPERTY, "Starts in server mode, the server will echo according HTTP GET-Requests." );
		final Flag theInitFlag = initFlag();
		final ConfigOption theConfigOption = configOption();
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag();
		final Flag theHelpFlag = helpFlag();
		final Flag theDebugFlag = debugFlag();

		// @formatter:off
		final Term theArgsSyntax = cases(
			and( thePingOption, optional( thePortOption, theHostOption, theConfigOption, theVerboseFlag, theDebugFlag ) ),
			and( theServerFlag, optional( thePortOption, theConfigOption, theVerboseFlag, theDebugFlag ) ),
			and( theInitFlag, optional( theConfigOption, theVerboseFlag, theDebugFlag) ),
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "To start the REST server", theServerFlag, thePortOption, theVerboseFlag ),
			example( "To call the REST server", theHostOption, thePortOption, thePingOption, theVerboseFlag ),
			example( "Run server by using the config file (more verbose)", theServerFlag, theConfigOption, theVerboseFlag ),
			example( "Initialize default config file", theInitFlag, theVerboseFlag),
			example( "Initialize specific config file", theConfigOption, theInitFlag, theVerboseFlag),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// <--| See "http://www.refcodes.org/refcodes/refcodes-cli"

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isVerbose = theCliHelper.isVerbose();
		final boolean isDebug = theArgsProperties.getBoolean( theDebugFlag );

		if ( isVerbose ) {
			LOGGER.info( "Starting application <" + NAME + "> ..." );
		}

		if ( isDebug ) {
			LOGGER.info( "Additional debug output enabled ..." );
		}

		if ( isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "Name: \"" + theArgsProperties.get( "application/name" ) + "\"" );
			LOGGER.info( "Company: \"" + theArgsProperties.get( "application/company" ) + "\"" );
			LOGGER.info( "Version: \"" + theArgsProperties.get( "application/version" ) + "\"" );
			LOGGER.printSeparator();
		}

		// ---------------------------------------------------------------------
		// REST:
		// ---------------------------------------------------------------------

		try {
			if ( theArgsProperties.getBoolean( theServerFlag ) ) {
				doServer( theArgsProperties );
			}
			else {
				doClient( theArgsProperties );
			}
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static void doServer( ApplicationProperties aProperties ) throws Exception {
		final int thePort = aProperties.getIntOr( "port", DEFAULT_PORT );
		if ( aProperties.getBoolean( "verbose" ) ) {
			LOGGER.info( "Starting server on port <" + thePort + "> ..." );
		}
		final HttpRestServer theServer = new HttpRestServer();
		theServer.onGet( "/ping/${text}", ( aRequest, aResponse ) -> {
			HttpBodyMap theBody = new HttpBodyMap();
			String thePing = aRequest.getWildcardReplacement( "text" );
			if ( thePing != null && thePing.length() != 0 ) {
				if ( aProperties.getBoolean( "verbose" ) ) {
					LOGGER.info( "Received ping \"" + thePing + "\", replying with pong \"" + thePing + "\"..." );
				}
				theBody.put( "pong", thePing );
				aResponse.setResponse( theBody );
			}
			else {
				theBody.put( "message", "Path element \"${ping}\" for path \"/ping/${ping}\" is missing!" );
				aResponse.setHttpStatusCode( HttpStatusCode.BAD_REQUEST );
			}
		} ).withOpen();

		theServer.onHttpException( new HttpExceptionHandler() {

			@Override
			public void onHttpError( RestRequestEvent aRequestEvent, HttpServerResponse aHttpServerResponse, Exception aException, HttpStatusCode aHttpStatusCode ) {
				if ( aProperties.getBoolean( "verbose" ) ) {
					LOGGER.warn( "Got a <" + aException.getClass().getSimpleName() + "> exception, responding with HTTP-Status-Code <" + aHttpStatusCode + "> (" + aHttpStatusCode.getStatusCode() + "> as of: " + aException.getMessage() );
				}
			}
		} );

		theServer.open( thePort );
		if ( aProperties.getBoolean( "verbose" ) ) {
			LOGGER.info( "Server started on port <" + thePort + "> ..." );
		}
		else {
			System.out.println( "Server started on port <" + thePort + "> ..." );
		}
	}

	private static void doClient( ApplicationProperties aProperties ) throws Exception {
		final int thePort = aProperties.getIntOr( "port", DEFAULT_PORT );
		final String thePing = aProperties.get( "text" );
		if ( thePing == null || thePing.isEmpty() ) {
			throw new IllegalArgumentException( "You provided an empty ping message, aborting!" );
		}
		final String theHost = aProperties.getOr( "host", DEFAULT_HOST );
		if ( aProperties.getBoolean( "verbose" ) ) {
			LOGGER.info( "Sending ping to host \"" + theHost + "\" on port <" + thePort + "> with message \"" + thePing + "\"..." );
		}
		final HttpRestClient theClient = new HttpRestClient();
		final RestResponse theResponse = theClient.doGet( Scheme.HTTP, theHost, thePort, "/ping/" + thePing );
		if ( theResponse.getHttpStatusCode().isErrorStatus() ) {
			throw theResponse.getHttpStatusCode().toHttpStatusException( theResponse.getHttpBody() );
		}
		final String thePong = theResponse.getResponse().get( "pong" );
		if ( aProperties.getBoolean( "verbose" ) ) {
			LOGGER.info( "Received pong from server with message \"" + thePong + "\"." );
		}
		else {
			System.out.println( "Pong = " + thePong );
		}
	}
}
