# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact hosts several [`REFCODES.ORG`](https://www.metacodes.pro/refcodes) related [`Maven Archetypes`](https://maven.apache.org/guides/introduction/introduction-to-archetypes.html) for quickly getting up and running with the [`REFCODES.ORG`](https://www.refcodes.org) toolkit.***

## Getting started ##

> Please refer to the [refcodes-archetype: Using the REFCODES.ORG toolkit made easy](https://www.metacodes.pro/refcodes/refcodes-archetype) documentation for an up-to-date and detailed description on the usage of this artifact. 

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<groupId>org.refcodes</groupId>
		<artifactId>refcodes-archetype-alt</artifactId>
		<version>3.0.0</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-archetype-alt). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-archetype-alt).

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/funcodez/funcodes-archetype/issues)
* Writing tests
* Code review
* Adding functionality
* Fixing bugs

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.