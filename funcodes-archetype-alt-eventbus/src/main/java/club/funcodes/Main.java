// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is written and provided by Siegfried Steiner, Munich, Germany.
// Feel free to use it as skeleton for your own applications. Make sure you have
// considered the license conditions of the included artifacts (pom.xml).
// -----------------------------------------------------------------------------
// The REFCODES.ORG artifacts used by this template are copyright (c) by
// Siegfried Steiner, Munich, Germany and licensed under the following
// (see "http://en.wikipedia.org/wiki/Multi-licensing") licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes;

import static org.refcodes.cli.CliSugar.*;
import static org.refcodes.cli.CliSugar.and;
import static org.refcodes.eventbus.ext.application.ApplicationBusSugar.*;
import static org.refcodes.eventbus.ext.application.ApplicationBusSugar.or;

import java.util.Random;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.ConfigOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.eventbus.ext.application.ApplicationBus;
import org.refcodes.eventbus.ext.application.ApplicationBusEvent;
import org.refcodes.eventbus.ext.application.ApplicationBusObserver;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;

/**
 * A minimum REFCODES.ORG enabled command line interface (CLI) application. Get
 * inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	// See "http://www.refcodes.org/blog/logging_like_the_nerds_log" |-->
	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();
	// <--| See "http://www.refcodes.org/blog/logging_like_the_nerds_log"

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "funcodes-archetype-alt-eventbus";
	private static final String TITLE = ( NAME.lastIndexOf( '-' ) != -1 ? "<" + NAME.substring( NAME.lastIndexOf( '-' ) + 1 ) + ">" : NAME ).toUpperCase();
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String DESCRIPTION = "A minimum REFCODES.ORG enabled eventbus (publish and subscribe) driven application. Get inspired by [https://bitbucket.org/funcodez].";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES (see [https://www.funcodes.club])";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	private static Random theRandom = new Random();

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		// See "http://www.refcodes.org/refcodes/refcodes-cli" |-->

		final Flag theInitFlag = initFlag();
		final ConfigOption theConfigOption = configOption();
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag();
		final Flag theHelpFlag = helpFlag();
		final Flag theDebugFlag = debugFlag();

		// @formatter:off
		final Term theArgsSyntax = cases(
			optional( theConfigOption, theVerboseFlag, theDebugFlag ),
			and( theInitFlag, optional( theConfigOption, theVerboseFlag, theDebugFlag) ),
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "Run application (more verbose)", theVerboseFlag ),
			example( "Run by using the config file (more verbose)", theConfigOption, theVerboseFlag ),
			example( "Initialize default config file", theInitFlag, theVerboseFlag),
			example( "Initialize specific config file", theConfigOption, theInitFlag, theVerboseFlag),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// <--| See "http://www.refcodes.org/refcodes/refcodes-cli"

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isVerbose = theCliHelper.isVerbose();
		final boolean isDebug = theArgsProperties.getBoolean( theDebugFlag );

		if ( isVerbose ) {
			LOGGER.info( "Starting application <" + NAME + "> ..." );
		}

		if ( isDebug ) {
			LOGGER.info( "Additional debug output enabled ..." );
		}

		if ( isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "Name: \"" + theArgsProperties.get( "application/name" ) + "\"" );
			LOGGER.info( "Company: \"" + theArgsProperties.get( "application/company" ) + "\"" );
			LOGGER.info( "Version: \"" + theArgsProperties.get( "application/version" ) + "\"" );
			LOGGER.printSeparator();
		}

		// ---------------------------------------------------------------------
		// EVENTBUS:
		// ---------------------------------------------------------------------

		final ApplicationBus theEventBus = parallelDispatchBus( true );

		try {
			// -----------------------------------------------------------------
			// PUBLISHER:
			// -----------------------------------------------------------------

			final ApplicationBusObserver theSportsObserver = new ApplicationBusObserver() {
				@Override
				public void onEvent( ApplicationBusEvent aEvent ) {
					LOGGER.info( "Got a sports event on channel <" + aEvent.getMetaData().getChannel() + "> ..." );
				}
			};

			final ApplicationBusObserver thePoliticsObserver = new ApplicationBusObserver() {
				@Override
				public void onEvent( ApplicationBusEvent aEvent ) {
					LOGGER.info( "Got a politics event on channel <" + aEvent.getMetaData().getChannel() + ">" + "> ..." );
				}
			};

			theEventBus.onInitialize( ( aEvent ) -> LOGGER.info( "Got an INITIALIZE event!" ) ); // Lifecycle
			theEventBus.onDestroy( ( aEvent ) -> LOGGER.info( "Got a DESTROY event!" ) ); // Lifecycle
			theEventBus.subscribe( or( channelEqualWith( "Soccer" ), channelEqualWith( "Football" ) ), theSportsObserver ); // Application
			theEventBus.subscribe( or( channelEqualWith( "War" ), channelEqualWith( "Education" ) ), thePoliticsObserver ); // Application

			// -----------------------------------------------------------------
			// PUBLISHER:
			// -----------------------------------------------------------------

			theEventBus.publishInitialize(); // Lifecycle

			for ( int i = 0; i < 100; i++ ) {
				switch ( theRandom.nextInt( 4 ) ) {
				case 0: {
					theEventBus.publishEvent( applicationBusEvent( "Education", theEventBus ) );
					break;
				}
				case 1: {
					theEventBus.publishEvent( applicationBusEvent( "War", theEventBus ) );
					break;
				}
				case 2: {
					theEventBus.publishEvent( applicationBusEvent( "Soccer", theEventBus ) );
					break;
				}
				case 3: {
					theEventBus.publishEvent( applicationBusEvent( "Football", theEventBus ) );
					break;
				}
				case 4: {
					theEventBus.publishEvent( applicationBusEvent( "Blowhard", theEventBus ) );
					break;
				}
				}
				// Thread.sleep( 250 );
			}
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}

		// Depending on the DispatchStrategy, the destroy event may occur quite early! |-->
		theEventBus.publishDestroy(); // Lifecycle
		// Depending on the DispatchStrategy, the destroy event may occur quite early! <--|
	}
}
