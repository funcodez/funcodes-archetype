// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes;

import org.refcodes.component.InitializeException;
import org.refcodes.component.LifecycleComponent;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;

public class Service implements LifecycleComponent {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Repository _repository;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	public Service( Repository aRepository ) {
		_repository = aRepository;
		LOGGER.info( toString() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize() throws InitializeException {
		LOGGER.printSeparator();
		LOGGER.info( "INITIALIZING [...]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() throws StartException {
		LOGGER.printSeparator();
		LOGGER.info( "STARTING [...]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pause() throws PauseException {
		LOGGER.printSeparator();
		LOGGER.info( "PAUSING [...]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resume() throws ResumeException {
		LOGGER.printSeparator();
		LOGGER.info( "RESUMING [...]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() throws StopException {
		LOGGER.printSeparator();
		LOGGER.info( "STOPPING [...]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		LOGGER.printSeparator();
		LOGGER.info( "DESTROYING [...]" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [repository=" + _repository + "]";
	}
}
