// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is written and provided by Siegfried Steiner, Munich, Germany.
// Feel free to use it as skeleton for your own applications. Make sure you have
// considered the license conditions of the included artifacts (pom.xml).
// -----------------------------------------------------------------------------
// The REFCODES.ORG artifacts used by this template are copyright (c) by
// Siegfried Steiner, Munich, Germany and licensed under the following
// (see "http://en.wikipedia.org/wiki/Multi-licensing") licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes;

import static org.refcodes.cli.CliSugar.*;

import org.refcodes.archetype.CtxHelper;
import org.refcodes.cli.ConfigOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.decoupling.InstanceMode;
import org.refcodes.decoupling.ext.application.ApplicationContext;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;

/**
 * A minimum REFCODES.ORG enabled command line interface (CLI) application. Get
 * inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	// See "http://www.refcodes.org/blog/logging_like_the_nerds_log" |-->
	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();
	// <--| See "http://www.refcodes.org/blog/logging_like_the_nerds_log"

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "funcodes-archetype-alt-decoupling";
	private static final String TITLE = ( NAME.lastIndexOf( '-' ) != -1 ? "<" + NAME.substring( NAME.lastIndexOf( '-' ) + 1 ) + ">" : NAME ).toUpperCase();
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String DESCRIPTION = "A minimum REFCODES.ORG enabled dependency injection and inversion of control application breaking up dependencies between components or modules of a software system. Get inspired by [https://bitbucket.org/funcodez].";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES (see [https://www.funcodes.club])";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		// See "http://www.refcodes.org/refcodes/refcodes-cli" |-->

		final Flag theInitFlag = initFlag();
		final ConfigOption theConfigOption = configOption();
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag();
		final Flag theHelpFlag = helpFlag();
		final Flag theDebugFlag = debugFlag();

		// @formatter:off
		final Term theArgsSyntax = cases(
			optional( theConfigOption, theVerboseFlag, theDebugFlag ),
			and( theInitFlag, optional( theConfigOption, theVerboseFlag, theDebugFlag) ),
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "Run application (more verbose)", theVerboseFlag ),
			example( "Run by using the config file (more verbose)", theConfigOption, theVerboseFlag ),
			example( "Initialize default config file", theInitFlag, theVerboseFlag),
			example( "Initialize specific config file", theConfigOption, theInitFlag, theVerboseFlag),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CtxHelper theCtxHelper = CtxHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		// <--| See "http://www.refcodes.org/refcodes/refcodes-cli"

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		try {

			// See "http://www.refcodes.org/refcodes/refcodes-decoupling" |-->

			theCtxHelper.addDependency( View.class );
			theCtxHelper.addDependency( Controller.class );
			theCtxHelper.addDependency( Service.class );
			theCtxHelper.addDependency( Repository.class );
			theCtxHelper.addConfiguration( Controller.Config.class, "controller" ).withInstanceMetrics( InstanceMode.SINGLETON_ON_DEMAND ); // Populate record from properties file
			theCtxHelper.addConfiguration( Repository.Config.class, "repository" ).withInstanceMetrics( InstanceMode.SINGLETON_ON_DEMAND ); // Populate record from properties file
			final ApplicationContext theCtx = theCtxHelper.createContext();
			if ( theCtxHelper.isVerbose() ) {
				// Show some insights on the created context |-->
				for ( Object e : theCtx.getInstances() ) {
					LOGGER.printSeparator();
					LOGGER.info( "Instance = " + e );
				}
				LOGGER.printTail();
				System.out.println( theCtx.toSchema() );
				// Show some insights on the created context <--|
			}

			// See "http://www.refcodes.org/refcodes/refcodes-decoupling" <--|

		}
		catch ( Throwable e ) {
			theCtxHelper.exitOnException( e );
		}
	}
}
