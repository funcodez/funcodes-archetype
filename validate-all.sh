# /////////////////////////////////////////////////////////////////////////////
# FUNCODES.DE
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being covered by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////
#!/bin/bash

# INIT:
CURRENT_PATH="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_PATH}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_PATH="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_PATH}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"
VALIDFATE_REACTOR="validate-all.txt"
REFCODES_PARENT_PATH="${SCRIPT_PATH}/../../org.refcodes/refcodes-parent"
MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f3- )"
if [ -z "${MODULE_NAME}" ]; then
	MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f2- )"
	if [ -z "${MODULE_NAME}" ]; then
		MODULE_NAME="${SCRIPT_DIR}"
	fi
fi
if [ -z ${COLUMNS} ] ; then
	export COLUMNS=$(tput cols)
fi

# ------------------------------------------------------------------------------
# ANSI ESCAPE CODES:
# ------------------------------------------------------------------------------

ESC_BOLD="\E[1m"
ESC_FAINT="\E[2m"
ESC_ITALIC="\E[3m"
ESC_UNDERLINE="\E[4m"
ESC_FG_RED="\E[31m"
ESC_FG_GREEN="\E[32m"
ESC_FG_YELLOW="\E[33m"
ESC_FG_BLUE="\E[34m"
ESC_FG_MAGENTA="\E[35m"
ESC_FG_CYAN="\E[36m"
ESC_FG_WHITE="\E[37m"
ESC_RESET="\E[0m"

# ------------------------------------------------------------------------------
# PRINTLN:
# ------------------------------------------------------------------------------

function printLn {
	char="-"
	if [[ $# == 1 ]] ; then
		char="$1"
	fi
	printf '%*s' "${COLUMNS:-$(tput cols)}" '' | tr ' ' ${char}
}

# ------------------------------------------------------------------------------
# QUIT:
# ------------------------------------------------------------------------------

function quit {
	input=""
	while ([ "$input" != "q" ] && [ "$input" != "y" ]); do
		echo -ne "> Continue? Enter [${ESC_BOLD}q${ESC_RESET}] to quit, [${ESC_BOLD}y${ESC_RESET}] to continue: ";
		read input;
	done
	# printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
	if [ "$input" == "q" ] ; then
		printLnt
		echo -e "> $(seColor "ESC_BOLD")Aborting due to user input.${ESC_RESET}"
		cd "${CURRENT_PATH}"
		exit
	fi
}

# ------------------------------------------------------------------------------
# BANNER:
# ------------------------------------------------------------------------------

function printBanner {
	banner=$( figlet -w 999 "/${MODULE_NAME}:>>>${SCRIPT_NAME}..." 2> /dev/null )
	if [ $? -eq 0 ]; then
		echo "${banner}" | cut -c -${COLUMNS}
	else
		banner "${SCRIPT_NAME}..." 2> /dev/null
		if [ $? -ne 0 ]; then
			echo -e "> ${SCRIPT_NAME}:" | tr a-z A-Z 
		fi
	fi
}

printBanner

# ------------------------------------------------------------------------------
# HELP:
# ------------------------------------------------------------------------------

function printHelp {
	printLn
	echo -e "Usage: ${ESC_BOLD}${SCRIPT_NAME}${ESC_RESET}.sh [-h]"
	printLn
	echo -e "Validates all REFCODES.ORG archetypes (as of <org.refcodes:refcodes-archetype-alt>)."
	printLn
	echo -e "${ESC_BOLD}-h${ESC_RESET}: Print this help"
	printLn
}

if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
	printHelp
	exit 0
fi

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

echo -e "> Building <${ESC_BOLD}${SCRIPT_DIR}${ESC_RESET}>..."
cd "${SCRIPT_PATH}"

function validate {
	archetypeId="${1}"
	artifactId="${archetypeId//refcodes/funcodes}"
	printLn
	echo -e "\n> Preparing archetype <${ESC_BOLD}${artifactId}${ESC_RESET}>..."
	echo -e "> Creating archetype <${ESC_BOLD}${artifactId}${ESC_RESET}> from <${ESC_BOLD}${archetypeId}${ESC_RESET}>..."
	if [[ ! -z "${artifactId}" ]] && [ -d "${SCRIPT_PATH}/${artifactId}" ] ; then
		rm -Rf "${artifactId}"
	fi
	result=$(mvn -B archetype:generate -DarchetypeGroupId=org.refcodes -DarchetypeArtifactId=${archetypeId} -DarchetypeVersion=${2} -DgroupId=club.funcodes -DartifactId="${artifactId}" -Dversion="${2}" -U)
	if [ $? -ne 0 ]; then
		echo -e "${result}"
		echo -e "> ${ESC_BOLD}Cannot create <${artifactId}> from <${archetypeId}>, aborting!${ESC_RESET}"
		cd "${CURRENT_PATH}"
		exit 1
	fi
	chmod ug+x ${SCRIPT_PATH}/${artifactId}/*.sh
	echo -e "> Validating archetype <${ESC_BOLD}${archetypeId}${ESC_RESET}>..."
	cd "${artifactId}"
	result=$(mvn clean install -U)
	if [ $? -ne 0 ]; then
		echo -e "${result}"
		echo -e "> ${ESC_BOLD}Archetype <${archetypeId}> has errors, aborting!${ESC_RESET}"
		cd "${CURRENT_PATH}"
		exit 1
	fi
	echo -e "> Archetype <${ESC_BOLD}${archetypeId}${ESC_RESET}> is ${ESC_FG_GREEN}OK${ESC_RESET}!"
	cd "${SCRIPT_PATH}"
}

version=$(xml2 < "${REFCODES_PARENT_PATH}/pom.xml" | grep "/project/version=")
version="${version#/project/version=}"
# version="3.0.9-SNAPSHOT"

echo -e "> Creating artifacts for REFCODES.ORG <${ESC_BOLD}${version}${ESC_RESET}> archetypes, previous artifacts will be deleted."
quit
while read archetypeId; do
	artifactId="${archetypeId//refcodes/funcodes}"
	if [[ ! -z "${artifactId}" ]] && [ -d "${SCRIPT_PATH}/${artifactId}" ] ; then
		rm -Rf "${artifactId}"
	fi
done < "${SCRIPT_PATH}/${VALIDFATE_REACTOR}"
cp "${SCRIPT_PATH}/pom.xml.master" "${SCRIPT_PATH}/pom.xml"
while read archetypeId; do
	validate "${archetypeId}" "${version}"
done < "${SCRIPT_PATH}/${VALIDFATE_REACTOR}"
# END:
cd "${CURRENT_PATH}"
printLn
echo -e "> All archetypes are <${ESC_BOLD}OK${ESC_RESET}>!"
